"""A module that exports a couple of functions to calculate
the following overpasses from satellites over a given location.

"""
from __future__ import annotations

import datetime as dt

import pandas as pd
from skyfield.api import load, utc, wgs84

CELESTRAK_URL = "https://celestrak.org/NORAD/elements/gp.php?GROUP=active&FO"


def get_satellites_objects(list_of_satellites, url):

    all_satellite = load.tle_file(url, filename="/tmp/satellite_list.txt")
    satellites = []
    for sel_sat in list_of_satellites:
        for satellite in all_satellite:
            if satellite.name == sel_sat:
                satellites.append(satellite)
    return satellites


def satellite_overpasses(
    lat: float,
    long: float,
    start_time: dt.datetime | None = None,
    altitude: float = 50.0,
    list_of_satellites: list | None = None,
    url: str = CELESTRAK_URL,
):
    """Calculate satellite overpasses over location and time.


    Args:
        lat (float): _description_
        long (float): _description_
        start_time (_type_, optional): _description_. Defaults to None.
        altitude (float, optional): _description_. Defaults to 50.0.
        list_of_satellites (list, optional): _description_. Defaults to [ "SENTINEL-3A", ].
        tmp_folder (str, optional): _description_. Defaults to "/tmp/".
        url (str, optional): _description_. Defaults to CELESTRAK_URL.

    Returns:
        _type_: _description_
    """
    if list_of_satellites is None:
        list_of_satellites = [
            "SENTINEL-3A",
            "SENTINEL-3B",
            "SENTINEL-2A",
            "SENTINEL-2B",
            "SENTINEL-5P",
            "LANDSAT 8",
            "LANDSAT 9",
            "NOAA 21 (JPSS-2)",
            "SUOMI NPP",
            "TERRA",
            "AQUA",
        ]
    satellites = get_satellites_objects(list_of_satellites, url)
    if start_time is None:
        start_time = dt.datetime.utcnow()
    start_time = start_time.replace(tzinfo=utc)

    ts = load.timescale()  # Set up a time scale
    location = wgs84.latlon(lat, long)  # Location of interest in Lat/Lon
    t0 = ts.utc(start_time)  # Start time
    t1 = ts.utc(start_time + dt.timedelta(days=5))  # End time
    a = []
    for satellite in satellites:
        times_list, _ = satellite.find_events(
            location, t0, t1, altitude_degrees=altitude
        )
        n_events = len(times_list) // 3
        a.append(
            [
                [
                    satellite.name,
                    times_list[3 * i].utc_strftime("%Y-%m-%d %H:%M:%S"),
                    times_list[(3 * i) + 2].utc_strftime("%Y-%m-%d %H:%M:%S"),
                ]
                for i in range(n_events)
            ]
        )

    df_overpass = pd.concat(
        [pd.DataFrame(x, columns=["Satellite", "Rise", "Set"]) for x in a]
    )
    df_overpass["Rise"] = pd.to_datetime(df_overpass.Rise)
    df_overpass["Set"] = pd.to_datetime(df_overpass.Set)
    df_overpass = df_overpass.sort_values(by="Rise", ascending=True)
    return df_overpass.reset_index(drop=True)
