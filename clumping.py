from __future__ import annotations

import concurrent
import datetime as dt
from dataclasses import dataclass, field

import numpy as np
import prosail
from skyfield.api import load, wgs84


@dataclass
class ThermalSail:
    """A data class to store thermal SAIL parameters.
    Note that the view/illumination geometry isn't stored
    here.
    """

    lam: float
    lai: float
    tsoil: float
    tleaf: float
    emv: float
    ems: float
    hspot: float
    lidfa: float
    tatm: float = field(default=-14)
    lidfb: float = field(default=None)
    tleaf_sunlit: float = field(default=None)
    tsoil_sunlit: float = field(default=None)

    def __post_init__(self):
        if self.tleaf_sunlit is None:
            self.tleaf_sunlit = self.tleaf
        if self.tsoil_sunlit is None:
            self.tsoil_sunlit = self.tsoil
        """_summary_
        """


def calculate_solar_position(
    longitude: float, latitude: float, time: dt.datetime | str
) -> tuple:
    """Calculate solar position for a given location and time.
    Longitude and latitude are in decimal degrees. Time can either
    be a string or a datetime object.


    Args:
        longitude (float): longitude in decimal degrees
        latitude (float): latitude in decimal degrees
        time (Union): time, either as datetime or string

    Returns:
        tuple: sza and saa in degrees
    """
    ts = load.timescale()
    t = ts.utc(
        time.year, time.month, time.day, time.hour, time.minute, time.second
    )
    planets = load("de421.bsp")
    earth, sun = planets["earth"], planets["sun"]
    loc = earth + wgs84.latlon(latitude, longitude)
    astro = loc.at(t).observe(sun)
    alt, az, _ = astro.apparent().altaz()
    return 90 - alt.degrees, az.degrees


# Load the JPL ephemeris DE421 (covers 1900-2050).
planets = load("de421.bsp")
earth, mars = planets["earth"], planets["mars"]


def calculate_bean_extinction(theta, lad):
    """Calculate bean extinction coefficient kappa
    according to Campbell & Norman

    Args:
        theta (float): View zenith angle (degrees)
        lad (float): Leaf angle distribution parameter. 1 for spherical

    Returns:
        float
    """
    theta = np.radians(theta)
    return np.sqrt(lad**2 + np.tan(theta) ** 2) / (
        lad + 1.774 * (lad + 1.182) ** -0.733
    )


def calculate_clumping(H, w, D, L, lad, theta, psi):
    """Calculates clumping parameter

    Args:
        H (float): Row height [m]
        w (float): Row width [m]
        D (float): Row separation [m]
        L (float): Leaf area index [m2/m2]
        lad (float): Leaf angle distribution  parameter. 1 for spherical
        theta (float): VZA (deg)
        psi (float): Relative azimuth angle between view and row orientation (deg)
    """
    # kappa (beam extinction)
    kappa_be = calculate_bean_extinction(theta, lad)
    # Apparent shaded fraction
    f = (
        w + H * np.tan(np.radians(theta)) * np.abs(np.sin(np.radians(psi)))
    ) / D
    f = np.minimum(1.0, f)  # Clip to f to be <=1
    # Canopy grap fraction
    gap_fraction = f * np.exp(-kappa_be * L) + (1.0 - f)
    return -np.log(gap_fraction) / (kappa_be * L)


def thermal_polar_sims(sza, parameters, vzas=None, raas=None):
    """Create a thermal polar plot for a given solar zenith angle,
    a SAIL parameter instance

    Args:
        sza (float): _description_
        parameters (ThermalSail): _description_
        vzas (iterable, optional): _description_. Defaults to np.linspace(0,90,5).
        raas (iterable, optional): _description_. Defaults to np.linspace(0,180, 30).

    Returns:
        _type_: _description_
    """
    if vzas is None:
        vzas = np.arange(0, 90, 5)
    if raas is None:
        raas = np.arange(0, 190, 10)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        index_futures = {}
        i = 0
        angles = []
        for vza in vzas:
            for raa in raas:
                future = executor.submit(
                    run_thermalsail, sza, vza, raa, parameters
                )
                futures.append(future)
                index_futures[future] = i
                angles.append([sza, vza, raa])
                i += 1

        results = [None] * len(futures)
        for future in concurrent.futures.as_completed(futures):
            index = index_futures[future]
            result = future.result()
            results[index] = result[1] - 273.15

        return angles, results


def run_thermalsail(sza, vza, raa, parameters):
    return run_thermal_sail(
        sza,
        vza,
        raa,
        parameters.lam,
        parameters.tsoil,
        parameters.tleaf,
        parameters.lai,
        parameters.lidfa,
        parameters.lidfb,
        parameters.hspot,
        parameters.emv,
        parameters.ems,
        tveg_sunlit=parameters.tleaf_sunlit,
        tsoil_sunlit=parameters.tsoil_sunlit,
        t_atm=parameters.tatm,
    )


def run_thermal_sail(
    sza,
    vza,
    raa,
    lam,
    tsoil,
    tveg,
    lai,
    lidfa,
    lidfb,
    hspot,
    emv,
    ems,
    tveg_sunlit=None,
    tsoil_sunlit=None,
    t_atm=-14,
):
    """Run SAIl simulations for a vegetation row scene. Basically, we modify LAI to be
    effective LAI, and calculate the clumping parameter in terms of having the equivalent
    grap fraction for a discontinuous canopy arranged in rectangular rows and a continuous
    equivalent canopy."""

    # Only two components
    if tveg_sunlit is None:
        tveg_sunlit = tveg
    if tsoil_sunlit is None:
        tsoil_sunlit = tsoil
    if lidfb is None:
        typelidf = 2
        lidfb = 0.0
    else:
        typelidf = 1
    return prosail.run_thermal_sail(
        lam,
        tveg + 273.15,
        tsoil + 273.15,
        tveg_sunlit + 273.15,
        tsoil_sunlit + 273.15,
        t_atm + 273.15,
        lai,
        lidfa,
        hspot,
        sza,
        vza,
        raa,
        emv=emv,
        ems=ems,
        lidfb=lidfb,
        typelidf=typelidf,
    )
