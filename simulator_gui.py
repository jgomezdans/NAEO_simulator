from __future__ import annotations

import datetime as dt
from dataclasses import dataclass

import clumping
import holoviews as hv
import NAEO_Flight as SD
import numpy as np
import pandas as pd
import panel as pn
from ipyleaflet import (
    GeoJSON,
    LayersControl,
    Map,
    Marker,
    SearchControl,
    WMSLayer,
    basemaps,
)
from satellite_overpass import satellite_overpasses

pn.extension()
pn.extension("ipywidgets")
pd.options.plotting.backend = "holoviews"


def do_sims(
    Lon,
    Lat,
    Time,
    lam,
    lai,
    tsoil,
    tleaf,
    emv,
    ems,
    hspot,
    lidfa,
    tleaf_sunlit,
    tsoil_sunlit,
    H,
    w,
    D,
    psi,
):
    """This is wrpaper function to do the simulations. Should also do optical...

    Args:
        Lon (_type_): _description_
        Lat (_type_): _description_
        Time (_type_): _description_
        lam (_type_): _description_
        lai (_type_): _description_
        tsoil (_type_): _description_
        tleaf (_type_): _description_
        emv (_type_): _description_
        ems (_type_): _description_
        hspot (_type_): _description_
        lidfa (_type_): _description_
        tleaf_sunlit (_type_): _description_
        tsoil_sunlit (_type_): _description_
        H (_type_): _description_
        w (_type_): _description_
        D (_type_): _description_
        psi (_type_): _description_

    Returns:
        _type_: _description_
    """
    sza, saa = clumping.calculate_solar_position(Lon, Lat, Time)
    sail_params = clumping.ThermalSail(
        lam=lam,
        lai=lai,
        tsoil=tsoil,
        tleaf=tleaf,
        emv=emv,
        ems=ems,
        hspot=hspot,
        lidfa=lidfa,
        lidfb=None,
        tleaf_sunlit=tleaf_sunlit,
        tsoil_sunlit=tsoil_sunlit,
    )
    vza = np.linspace(-90, 90, 90)
    raa = [0 if v <= 0 else 180 for v in vza]
    retval = [
        clumping.run_thermalsail(sza, abs(v), r, sail_params)
        for v, r in zip(vza, raa)
    ]
    retval = np.array([x[1] - 273.15 for x in retval])
    scatter = hv.Scatter(
        (vza, retval), "VZA [deg]", "Brightness temperature [degC]"
    ).opts(width=800)
    curve = hv.Curve(scatter, label="Continuous canopy")
    retval = curve * scatter
    p = clumping.ThermalSail(**sail_params.__dict__.copy())
    row_sims = []
    for v, r in zip(vza, raa):
        Omega = clumping.calculate_clumping(
            H, w, D, lai, sail_params.lidfa, v, psi
        )
        p.lai = lai * np.abs(Omega)
        x = clumping.run_thermalsail(sza, abs(v), r, p)
        row_sims.append(x[1] - 273.15)
    scatter = hv.Scatter(
        (vza, row_sims), "VZA [deg]", "Brightness temperature [degC]"
    ).opts(width=800)
    curve = hv.Curve(scatter, label="Row-oriented canopy")

    return retval * scatter * curve


# The following are just storage objects for the GUI, so it's easier to
# access individual widgets via some kind of class hierarchy


@dataclass
class AcquisitionGUI:
    lon: pn.widgets.FloatInput
    lat: pn.widgets.FloatInput
    timer: pn.widgets.DatetimePicker
    lam: pn.widgets.FloatSlider
    GUI: pn.WidgetBox


@dataclass
class ArchitectureGUI:
    hspot: pn.widgets.FloatSlider
    lidfa: pn.widgets.FloatSlider
    lai: pn.widgets.FloatSlider
    GUI: pn.WidgetBox


@dataclass
class ThermodynamicsGUI:
    tsoil: pn.widgets.FloatSlider
    tleaf: pn.widgets.FloatSlider
    tsoil_sunlit: pn.widgets.FloatSlider
    tleaf_sunlit: pn.widgets.FloatSlider
    ems: pn.widgets.FloatSlider
    emv: pn.widgets.FloatSlider
    GUI: pn.WidgetBox


@dataclass
class RowGUI:
    row_H: pn.widgets.FloatSlider
    row_width: pn.widgets.FloatSlider
    row_sep: pn.widgets.FloatSlider
    row_angle: pn.widgets.FloatSlider
    GUI: pn.WidgetBox


# The following functions define the actual GUI widgets and
# set them up. These functions should probably have sensible
# defaults for the different sliders and pararm boundaries
def create_acquisitions_gui():
    # Acquisition
    lon = pn.widgets.FloatInput(
        name="Longitude", start=-180, end=180, value=11.124
    )
    lat = pn.widgets.FloatInput(
        name="Latitude", start=-90, end=90, value=42.7635
    )
    timer = pn.widgets.DatetimePicker(
        name="Overflight time", value=dt.datetime(2023, 5, 18, 14, 0)
    )
    lam = pn.widgets.FloatSlider(
        name="Wavelength [um]",
        start=7.6,
        end=12.5,
        step=(12.5 - 7.6) / 103,
        value=9.5,
    )
    acquisition = pn.WidgetBox("### Acquisition", lon, lat, timer, lam)
    return AcquisitionGUI(lon, lat, timer, lam, acquisition)


def create_architecture_gui():
    # Architecture
    hspot = pn.widgets.FloatSlider(
        name="HotSpot parameter [-]", start=0.01, end=0.1, value=0.05
    )
    lidfa = pn.widgets.FloatSlider(
        name="Average leaf angle [deg]", start=0.0, end=90, value=45
    )
    lai = pn.widgets.FloatSlider(
        name="Leaf Area Index [m2/m2]", start=0.0, end=4, value=2
    )
    architecture = pn.WidgetBox("### Canopy", lai, lidfa, hspot)
    return ArchitectureGUI(hspot, lidfa, lai, architecture)


def create_thermodynamics_gui():
    # Thermodynamics
    tsoil = pn.widgets.FloatSlider(
        name="Shaded soil temperature [degC]", start=-10, end=100, value=35
    )
    tleaf = pn.widgets.FloatSlider(
        name="Shaded leaf temperature [degC]", start=-10, end=100, value=25
    )
    tsoil_sunlit = pn.widgets.FloatSlider(
        name="Sunlit soil temperature [degC]", start=-10, end=100, value=45
    )
    tleaf_sunlit = pn.widgets.FloatSlider(
        name="Sunlit leaf temperature [degC]", start=-10, end=100, value=33
    )
    ems = pn.widgets.FloatSlider(
        name="Soil emissivity [-]", start=0.9, end=1.0, value=0.94
    )
    emv = pn.widgets.FloatSlider(
        name="Leaf emissivity [-]", start=0.9, end=1.0, value=0.98
    )
    thermodynamics = pn.WidgetBox(
        "### Thermal", tsoil, tleaf, tsoil_sunlit, tleaf_sunlit, ems, emv
    )
    return ThermodynamicsGUI(
        tsoil, tleaf, tsoil_sunlit, tleaf_sunlit, ems, emv, thermodynamics
    )


def create_row_gui():

    row_H = pn.widgets.FloatSlider(
        name="Row height [m]", start=0.0, end=5.0, value=1.5
    )
    row_width = pn.widgets.FloatSlider(
        name="Row width [m]", start=0.0, end=5.0, value=0.5
    )
    row_sep = pn.widgets.FloatSlider(
        name="Row separation [m]", start=0.0, end=10.0, value=1.2
    )
    row_angle = pn.widgets.FloatSlider(
        name="Row angle with VAA [deg]", start=0, end=90.0, value=45
    )
    rows = pn.WidgetBox(
        "### Row geometry", row_H, row_width, row_sep, row_angle
    )
    return RowGUI(row_H, row_width, row_sep, row_angle, rows)


# This is the main Sidebar GUI class. Basically, it defines the sidebar
# widgets and instantiates them, leaving them ready to use.
@dataclass
class SidebarGUI:
    acquisition: AcquisitionGUI = create_acquisitions_gui()
    architecture: ArchitectureGUI = create_architecture_gui()
    thermodynamics: ThermodynamicsGUI = create_thermodynamics_gui()
    rows: RowGUI = create_row_gui()
    GUI: pn.Row = None


def __post_init__(self):
    if self.acquisition is None:
        self.acquisition = create_acquisitions_gui()
    if self.architecture is None:
        self.architecture = create_architecture_gui()
    if self.thermodynamics is None:
        self.thermodynamics = create_thermodynamics_gui()
    if self.rows is None:
        self.rows = create_row_gui()
    if self.GUI is None:
        self.GUI = pn.Row(
            self.acquisition,
            self.architecture,
            self.thermodynamics,
            self.rows,
        )


# this class stores the location map and location marker
class LocationMap:
    """A class to store the ipyleaflet maps"""

    def __init__(self, location=(42.7465, 11.1124)):
        self.m = Map(
            center=location,
            basemap=basemaps.OpenStreetMap.Mapnik,
            zoom_snap=0.25,
            zoom=12,
        )

        wms = WMSLayer(
            url="https://tiles.maps.eox.at/wms",
            layers="s2cloudless-2020_3857",
            format="image/jpeg",
            transparent=True,
            attribution="Sentinel2 Cloudless 2020",
            name="Sentinel 2 mosaic",
        )

        self.marker = Marker(
            location=location, draggable=True, name="Location"
        )
        self.m.add_control(
            SearchControl(
                position="topleft",
                url="https://nominatim.openstreetmap.org/search?format=json&q={s}",
                zoom=12,
                marker=self.marker,
            )
        )
        self.m.add_layer(self.marker)
        self.m.add_layer(wms)
        self.m.add_control(LayersControl(position="topright"))


class AppGUI:
    """The man app GUI. This sets everything up, widgets, maps, etc.
    All state is defined in the class, so you can access individual
    widgets via `self.sidebar_gui.acquistion.timer` e.g.
    """

    def __init__(self):
        self.location_map = LocationMap()
        self.sidebar_gui = SidebarGUI()
        self.inputfile = self._initial_dataframe()
        self._overpasses()
        self.plot_flight_lines()
        self.df_widget = pn.widgets.DataFrame(
            self.inputfile, name="Fligth Plan", heigh=250
        )
        self.df_overpasses = pn.widgets.DataFrame(
            self.overpasses, name="Satellite overpasses", heigh=250
        )
        self.json_widget = self._info_widget()
        self.sidebar_gui.acquisition.timer.param.watch(
            self.on_time_changed, "value"
        )
        self.location_map.marker.observe(self.on_location_changed, "location")
        self.df_widget.param.watch(self.on_df_change, "value")
        self.interactive_simulations = pn.bind(
            do_sims,
            self.sidebar_gui.acquisition.lon,
            self.sidebar_gui.acquisition.lat,
            self.sidebar_gui.acquisition.timer,
            self.sidebar_gui.acquisition.lam,
            self.sidebar_gui.architecture.lai,
            self.sidebar_gui.thermodynamics.tsoil,
            self.sidebar_gui.thermodynamics.tleaf,
            self.sidebar_gui.thermodynamics.emv,
            self.sidebar_gui.thermodynamics.ems,
            self.sidebar_gui.architecture.hspot,
            self.sidebar_gui.architecture.lidfa,
            self.sidebar_gui.thermodynamics.tleaf_sunlit,
            self.sidebar_gui.thermodynamics.tsoil_sunlit,
            self.sidebar_gui.rows.row_H,
            self.sidebar_gui.rows.row_width,
            self.sidebar_gui.rows.row_sep,
            self.sidebar_gui.rows.row_angle,
        )
        self.template = pn.template.FastListTemplate(
            site="NCEO Airborne Facility",
            title="Brightness temperature simulations",
            sidebar=pn.Row(
                self.sidebar_gui.acquisition.GUI,
                self.sidebar_gui.architecture.GUI,
                self.sidebar_gui.thermodynamics.GUI,
                self.sidebar_gui.rows.GUI,
            ),
            main=pn.Column(
                pn.layout.Row(
                    self.location_map.m,
                    pn.layout.Tabs(
                        self.df_widget,
                        self.df_overpasses,
                    ),
                    height=500,
                ),
                pn.Row(
                    self.interactive_simulations,
                    self.json_widget,
                    # pn.state.log_terminal,
                    sizing_norm="stretch_both",
                    height=200,
                ),
            ),
            accent_base_color="#0a2d50",
            header_background="#e2231a",
            logo="./dastardly-and-muttley-in-their-flying-machines-586a963662f6f.png",
        ).servable()

    def _info_widget(self):
        sza, saa = clumping.calculate_solar_position(
            self.sidebar_gui.acquisition.lon.value,
            self.sidebar_gui.acquisition.lat.value,
            self.sidebar_gui.acquisition.timer.value,
        )

        return pn.pane.JSON(
            {
                "SZA": f"{sza:6.2f}",
                "SAA": f"{saa:6.2f}",
                "Lon": f"{self.sidebar_gui.acquisition.lon.value:+8.5f}",
                "Lat": f"{self.sidebar_gui.acquisition.lat.value:+8.5f}",
                "UTC": self.sidebar_gui.acquisition.timer.value.strftime(
                    "%Y-%m-%d %H:%M:%S"
                ),
            },
            height=300,
            width=400,
            name="Sun position",
        )

    def _initial_dataframe(self) -> pd.DataFrame:
        """An initial data frame for the flightlines"""
        return pd.DataFrame(
            {
                "Flight Line": range(1, 10),
                "Ident": 3 * ["LSTM"] + 3 * ["SP"] + 3 * ["AZ"],
                "VZA": 3 * [0, 36, -36],
                "backward/(Y/N)": 9
                * [
                    None,
                ],
                "Time/UTC": 9
                * [
                    self.sidebar_gui.acquisition.timer.value.strftime(
                        "%H:%M"
                    ),
                ],
            }
        )

    def _overpasses(self):

        self.overpasses = satellite_overpasses(
            self.sidebar_gui.acquisition.lat.value,
            self.sidebar_gui.acquisition.lon.value,
            self.sidebar_gui.acquisition.timer.value,
        )

    def plot_flight_lines(self):
        flight_geojson = SD.do_LSTM_flight_lines(
            self.inputfile,
            "Grosseto",
            self.sidebar_gui.acquisition.timer.value.strftime("%Y-%m-%d"),
            self.sidebar_gui.acquisition.lat.value,
            self.sidebar_gui.acquisition.lon.value,
            14.52,
            100,
            False,
            True,
            24.2,
        )

        flight_lines = []
        flight_swaths = []
        colours = ["Yellow", "Red", "White"]
        for i, feat in enumerate(flight_geojson):
            for fl in feat["features"]:
                if fl["geometry"]["type"] == "Polygon":
                    gg = GeoJSON(
                        data=fl,
                        style={
                            "color": colours[i],
                            "opacity": 0.2,
                            "weight": 3,
                            "fillOpacity": 0.2,
                        },
                        name=fl["properties"]["name"],
                    )
                    flight_swaths.append(gg)
                elif fl["geometry"]["type"] == "LineString":
                    gg = GeoJSON(
                        data=fl,
                        style={
                            "color": colours[i],
                            "opacity": 1,
                            "weight": 3,
                            "fillOpacity": 1,
                        },
                        name=fl["properties"]["name"],
                    )
                    flight_lines.append(gg)
        if hasattr(self, "flight_lines"):
            for gg, nn in zip(self.flight_lines, flight_lines):
                gg.data = nn.data
        else:
            self.flight_lines = flight_lines
            [self.location_map.m.add_layer(gg) for gg in self.flight_lines]
        if hasattr(self, "flight_swaths"):
            for gg, nn in zip(self.flight_swaths, flight_swaths):
                gg.data = nn.data
        else:
            self.flight_swaths = flight_swaths
            [self.location_map.m.add_layer(gg) for gg in self.flight_swaths]

        # def hover_handler(event=None, feature=None, id=None, properties=None):
        #    message.value = properties["Name"]
        # geo_json.on_hover(hover_handler)

    def on_location_changed(self, event):
        new = event["new"]
        self.sidebar_gui.acquisition.lon.value = new[1]
        self.sidebar_gui.acquisition.lat.value = new[0]
        sza, saa = clumping.calculate_solar_position(
            self.sidebar_gui.acquisition.lon.value,
            self.sidebar_gui.acquisition.lat.value,
            self.sidebar_gui.acquisition.timer.value,
        )
        self.json_widget.object = {
            "SZA": f"{sza:6.2f}",
            "SAA": f"{saa:6.2f}",
            "Lon": f"{self.sidebar_gui.acquisition.lon.value:+6.2f}",
            "Lat": f"{self.sidebar_gui.acquisition.lat.value:+6.2f}",
            "UTC": self.sidebar_gui.acquisition.timer.value.strftime(
                "%Y-%m-%d %H:%M:%s"
            ),
        }
        self.plot_flight_lines()
        self._overpasses()
        self.df_overpasses.value = self.overpasses

    def on_time_changed(self, value):
        sza, saa = clumping.calculate_solar_position(
            self.sidebar_gui.acquisition.lon.value,
            self.sidebar_gui.acquisition.lat.value,
            self.sidebar_gui.acquisition.timer.value,
        )
        self.json_widget.object = {
            "SZA": f"{sza:6.2f}",
            "SAA": f"{saa:6.2f}",
            "Lon": f"{self.sidebar_gui.acquisition.lon.value:+6.2f}",
            "Lat": f"{self.sidebar_gui.acquisition.lat.value:+6.2f}",
            "UTC": self.sidebar_gui.acquisition.timer.value.strftime(
                "%Y-%m-%d %H:%M:%s"
            ),
        }
        self.plot_flight_lines()
        self._overpasses()
        self.df_overpasses.value = self.overpasses

    def on_df_change(self, value):
        self.plot_flight_lines()

    # def on_overpass_change(self, value):
    #
    #    self.df_overpasses.value = self._overpasses()
