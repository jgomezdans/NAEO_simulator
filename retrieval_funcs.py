from __future__ import annotations

import clumping
import numpy as np
import prosail
import scipy.stats as ss


def gaussian(x, x0, s=4):
    y = (
        1.0
        / np.sqrt(2.0 * np.pi * s**2)
        * np.exp(-((x - x0) ** 2) / (2.0 * s**2))
    )
    y[y <= 0.005] = 0.0
    return y / sum(y)


def define_fenix_spectral_responses():
    wvc = np.loadtxt("fenix1k_wavebands.txt")
    wv = np.arange(400, 2501)

    return (
        wvc,
        np.array([np.fromiter((gaussian(wv, lc, 5)), float) for lc in wvc]),
    )


def fenix_sims(
    sza: float,
    vza: float,
    raa: float,
    prosail_params: tuple,
    spectral_responses: np.ndarray,
):
    (
        n,
        cab,
        car,
        cbrown,
        cw,
        cm,
        lai,
        lidfa,
        hspot,
        lidfb,
        rsoil,
        psoil,
    ) = prosail_params
    if lidfb is not None:
        simu_refl = prosail.run_prosail(
            n,
            cab,
            car,
            cbrown,
            cw,
            cm,
            lai,
            lidfa,
            hspot,
            sza,
            vza,
            raa,
            typelidf=1,
            lidfb=lidfb,
            rsoil=rsoil,
            psoil=psoil,
        )
    else:
        # Campbell LAD
        simu_refl = prosail.run_prosail(
            n,
            cab,
            car,
            cbrown,
            cw,
            cm,
            lai,
            lidfa,
            hspot,
            sza,
            vza,
            raa,
            typelidf=2,
            lidfb=None,
            rsoil=rsoil,
            psoil=psoil,
        )
    simu_bands = simu_refl[None, :] * spectral_responses
    return simu_bands.sum(axis=1)


def extract_params_from_vector(x: list, lams: list):
    """Extract model parameters from assimilation control vector. Requires the thermal
    wavelengths to extract the spectral emissivities for soil and leaves

    """
    n_bands = len(lams)
    (
        n,
        cab,
        cbrown,
        cm,
        cw,
        lai,
        lidfa,
        rsoil,
        psoil,
        tsoil,
        tleaf,
        tsoil_sunlit,
        tleaf_sunlit,
    ) = x[:13]
    emss = x[13 : (13 + n_bands)]
    emvs = x[-n_bands:]

    car = cbrown * 0.25
    hspot = 0.05
    lidfb = None
    cab = -100 * np.log(cab)
    cm = (-1.0 / 100) * np.log(cm)
    lai = -2 * np.log(lai)
    lidfa = 90 * lidfa
    cw = (-1 / 50.0) * np.log(cw)

    optical_params = [
        n,
        cab,
        car,
        cbrown,
        cm,
        cw,
        lai,
        lidfa,
        hspot,
        lidfb,
        rsoil,
        psoil,
    ]
    thermal_params = [
        lai,
        lidfa,
        tsoil,
        tleaf,
        tsoil_sunlit,
        tleaf_sunlit,
        *list(emss),
        *list(emvs),
    ]
    all_params = [
        n,
        cab,
        cbrown,
        cm,
        cw,
        lai,
        lidfa,
        rsoil,
        psoil,
    ] + thermal_params[2:]

    return optical_params, thermal_params, all_params


def optical_loglike(
    prosail_params: tuple,
    sza: list,
    vza: list,
    raa: list,
    rho_obs: np.ndarray,
    rho_sigma: np.ndarray,
    spectral_responses: np.ndarray,
    return_fwd: bool = False,
) -> float:
    """Optical log-likelihood calculations using PROSAIL

    TODO Check the shapes of rho_obs, rho_sigma, spectral_responses and the angles!
    """

    err = np.zeros_like(rho_obs)
    rho_sim = np.zeros_like(rho_obs)
    for i, (s, v, r) in enumerate(zip(sza, vza, raa)):
        rho_pred = fenix_sims(s, v, r, prosail_params, spectral_responses)
        err[i, :] = (rho_pred - rho_obs[i, :]) ** 2 / rho_sigma[i, :] ** 2
        rho_sim[i, :] = rho_pred
    # Return the log-likelihood
    opt_log_likelihood = -0.5 * err.sum()

    if return_fwd:
        return (
            opt_log_likelihood
            if np.isfinite(opt_log_likelihood)
            else -np.inf,
            rho_sim,
        )

    return opt_log_likelihood if np.isfinite(opt_log_likelihood) else -np.inf


def thermal_loglike(
    params, lams, sza, vza, raa, bt_obs, bt_sigma, return_fwd=False
):
    sail_params = clumping.ThermalSail(
        lam=9.5,
        lai=params[0],
        tsoil=params[2],
        tleaf=params[3],
        emv=0.98,
        ems=0.94,
        hspot=0.05,
        lidfa=params[1],
        tsoil_sunlit=params[4],
        tleaf_sunlit=params[5],
    )
    n_bands = len(lams)
    emss = params[6 : (6 + n_bands)]
    emvs = params[-n_bands:]
    err = 0.0
    sim_obs = np.zeros_like(bt_obs)
    for i, L in enumerate(lams):
        sail_params.lam = L
        sail_params.emv = emvs[i]
        sail_params.ems = emss[i]
        for j, (v, s, r) in enumerate(zip(vza, sza, raa)):
            retval = (
                clumping.run_thermalsail(s, v, r, sail_params)[1] - 273.15
            )
            err += (bt_obs[i, j] - retval) ** 2 / (bt_sigma[i, j] ** 2)
            sim_obs[i, j] = retval

    if np.isfinite(err):
        return -0.5 * err
    return -np.inf


def logprior(
    params, mean_prior_params, sigma_prior_params, min_params, max_params
):
    params = np.array(params)
    if np.any(params <= min_params):
        return -np.inf
    if np.any(params >= max_params):
        return -np.inf

    return sum(
        ss.norm(loc=mean_prior_params[i], scale=sigma_prior_params[i]).logpdf(
            params[i]
        )
        for i in range(len(params))
    )


def log_posterior(
    x,
    rho_obs,
    bt_obs,
    rho_sigma,
    bt_sigma,
    rho_angles,
    bt_angles,
    bt_spectral,
    rho_spectral,
    mean_prior_params,
    sigma_prior_params,
    min_params,
    max_params,
):
    optical_params, thermal_params, all_params = extract_params_from_vector(
        x, bt_spectral
    )

    log_prior = logprior(
        all_params,
        mean_prior_params,
        sigma_prior_params,
        min_params,
        max_params,
    )
    if not np.isfinite(log_prior):
        return -np.inf

    optical_loglikelihood = optical_loglike(
        optical_params,
        rho_angles[0],
        rho_angles[1],
        rho_angles[2],
        rho_obs,
        rho_sigma,
        rho_spectral,
    )

    if not np.isfinite(optical_loglikelihood):
        return -np.inf

    thermal_loglikelihood = thermal_loglike(
        thermal_params,
        bt_spectral,
        bt_angles[0],
        bt_angles[1],
        bt_angles[2],
        bt_obs,
        bt_sigma,
    )

    if not np.isfinite(thermal_loglikelihood):
        return -np.inf
    scale_optical = sum(rho_obs.shape)
    scale_thermal = sum(bt_obs.shape)
    return (
        optical_loglikelihood / scale_optical
        + thermal_loglikelihood / scale_thermal
        + log_prior
    )
