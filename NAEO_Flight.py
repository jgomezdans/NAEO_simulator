"""
Collection of functions in Python to automate planning of flight lines
Will eventually push to GitHub.



C. Middleton 03/04/2022
"""
from __future__ import annotations

import datetime as dt
import math
import os
from datetime import datetime, timedelta
from math import cos, radians, sin, sqrt, tan
from pathlib import Path

import geojson
import haversine as H
import numpy as np

# import calender
import pandas as pd
import pytz
import simplekml
from astral.location import Location, LocationInfo
from clumping import calculate_solar_position
from zoneinfo import ZoneInfo

os.environ["OGR_GEOMETRY_ACCEPT_UNCLOSED_RING"] = "NO"

# =================================================================================================================
"""
Conversion Functions

"""


def create_kml(
    FL_Name_list,
    start_long_list,
    start_lat_list,
    height_list,
    end_long_list,
    end_lat_list,
    start_left_long_list,
    start_left_lat_list,
    start_right_long_list,
    start_right_lat_list,
    end_left_long_list,
    end_left_lat_list,
    end_right_long_list,
    end_right_lat_list,
):
    kml = simplekml.Kml()
    c = 0
    for i in range(len(FL_Name_list)):
        ls = kml.newlinestring(name=FL_Name_list[c])
        ls.coords = [
            (start_long_list[c], start_lat_list[c], height_list[c]),
            (end_long_list[c], end_lat_list[c], height_list[c]),
        ]
        ls.altitudemode = simplekml.AltitudeMode.relativetoground
        # ls.altitudemode =simplekml.AltitudeMode.clamptoground
        ls.style.linestyle.width = 2
        ls.style.linestyle.color = simplekml.Color.red

        # Define Swath Polygon
        point1 = (start_left_long_list[c], start_left_lat_list[c])
        point2 = (start_right_long_list[c], start_right_lat_list[c])
        point3 = (end_right_long_list[c], end_right_lat_list[c])
        point4 = (end_left_long_list[c], end_left_lat_list[c])

        pol = kml.newpolygon(
            name="%s_swath" % FL_Name_list[c],
            outerboundaryis=[(point3), (point4), (point1), (point2)],
        )
        pol.style.polystyle.color = "990000ff"  # transparent red
        pol.style.polystyle.outline = 1
        pol.altitudemode = simplekml.AltitudeMode.clamptoground
        c = c + 1
    return kml


def create_geojson(
    FL_Name_list,
    start_long_list,
    start_lat_list,
    height_list,
    end_long_list,
    end_lat_list,
    start_left_long_list,
    start_left_lat_list,
    start_right_long_list,
    start_right_lat_list,
    end_left_long_list,
    end_left_lat_list,
    end_right_long_list,
    end_right_lat_list,
):

    line_feats = []
    polys = []
    for c, name in enumerate(FL_Name_list):
        line = geojson.LineString(
            [
                (start_long_list[c], start_lat_list[c], height_list[c]),
                (end_long_list[c], end_lat_list[c], height_list[c]),
            ]
        )
        line_tmp = geojson.Feature(geometry=line, properties={"name": name})

        line_feats.append(line_tmp)
        point1 = (start_left_long_list[c], start_left_lat_list[c])
        point2 = (start_right_long_list[c], start_right_lat_list[c])
        point3 = (end_right_long_list[c], end_right_lat_list[c])
        point4 = (end_left_long_list[c], end_left_lat_list[c])

        poly = geojson.Polygon([[point1, point2, point3, point4, point1]])
        poly_tmp = geojson.Feature(
            geometry=poly, properties={"name": f"swath {name}"}
        )

        polys.append(poly_tmp)
    return geojson.FeatureCollection(line_feats + polys)


def conv_aircraft_speed(speed, conversion):
    """
    Converts aircraft speed

    """
    if conversion == "knots_to_m/s":
        speed = speed * 0.514444
    elif conversion == "m/s_to_knots":
        speed = speed / 0.514444
    else:
        moan = (
            "Invalid conversion string:"
            ' must be either "knots_to_m/s" or "m/s_to_knots"'
        )
        raise ValueError(moan)
        return 0
    return speed


def datetoDOY(date, leap):
    """
    Converts the date (string, "YYYY-MM-DD") to day of the year

    Inputs: date, "yes" or "no" string denoting leap year

    Note: requires LUTs to work
    """
    date_split = date.split("-")  # split according to dash
    date_dd = date_split[2]
    date_mm = date_split[1]
    date_yyyy = date_split[0]
    date_yy = date_yyyy[2:4]

    if leap == "no":
        DOY_table = pd.read_csv("JD_non-leap.csv")
    else:
        DOY_table = pd.read_csv("JD_leap.csv")

    headers = list(DOY_table.columns.values)
    month = headers[int(date_mm)]
    month = DOY_table[month]
    day = month[int(int(date_dd) - 1)]

    # Get string form and output both as a list
    daystring = "%d" % day
    if len(daystring) < 3:
        daystring = "0" + daystring
    yearstring = daystring + "-" + date_yy
    return [day, yearstring]


def deg_to_degString(Lat, Long, output_type):
    """
    Converts decimal degrees to degrees decimal mins in single string
    Output format example: "DD:MM N DD:MM E"
    """
    ind_lat = "S" if Lat < 0 else "N"

    ind_long = "W" if Long < 0 else "E"

    lat_deg = int(sqrt(int(Lat) ** 2))
    lat_min = round((Lat % 1) * 60, 3)
    lat_sec = round((lat_min % 1) * 60, 3)

    long_deg = int(sqrt(int(Long) ** 2))
    long_min = round((Long % 1) * 60, 3)
    long_sec = round((long_min % 1) * 60, 3)

    if output_type == "dec_deg":
        lat_long_string = f"{Lat} {Long}"
        # print(lat_long_string)

    elif output_type == "deg_decmin":
        # CHECK THIS - IT IS VERY BROKEN
        lat_long_string = "{}{} {}'{} {}{}{}'{}".format(
            lat_deg,
            chr(176),
            lat_min,
            ind_lat,
            long_deg,
            chr(176),
            long_min,
            ind_long,
        )
    elif output_type == "deg_min_sec":
        # CHECK THIS - IT IS VERY BROKEN
        lat_long_string = "{}{} {}' {}\" {} {}{} {}' {}\" {}".format(
            lat_deg,
            chr(176),
            int(lat_min),
            lat_sec,
            ind_lat,
            long_deg,
            chr(176),
            int(long_min),
            long_sec,
            ind_long,
        )
    else:
        moan = (
            "Invalid output type string - must be deg_decmin or dec_min_sec"
        )
        raise ValueError(moan)
    return lat_long_string


def conv_metToFeet(length, conv_string):
    """
    Converts metres to feet or feet to metres, depending on conv_string

    Inputs: length, conv_string = "met_to_feet" or "feet_to_met"
    """

    if conv_string == "feet_to_met":
        new_length = length * 0.3048
    elif conv_string == "met_to_feet":
        new_length = length * 3.28084

    else:
        moan = (
            "Invalid input: conv_string must equal "
            'either "met_to_feet" or "feet_to_met"'
        )
        raise ValueError(moan)
    return new_length


# =================================================================================================================
"""
Data Table Generation

"""


def datetime_range(start, end, delta):
    """
    Create the time series every delta
    From: "https://stackoverflow.com/questions/39298054/generating-15-minute-time-interval-array-in-python"
    """
    current = start
    while current < end:
        yield current
        current += delta


def genSolAzTable(Loc, Country, Region, lat, long, strDate, dst):
    """
    Uses Astral python Library to generate a LUT of the solar azimuth angle every 5
    minutes. Note output times are in Local Time here.

    Saves table as csv
    """
    # Lob in the Location information
    L = LocationInfo(Loc, Country, Region, lat, long)
    # Strip date from the string
    date = strDate.split("-")
    year = int(date[0])
    month = int(date[1])
    day = int(date[2])

    # Generate Time series as a list (every 5 mins)
    times = [
        dt.strftime("%Y-%m-%d %H:%M:%S")
        for dt in datetime_range(
            datetime(year, month, day, 8),
            datetime(year, month, day, 20),
            timedelta(minutes=5),
        )
    ]

    dt = [
        datetime.fromisoformat(t).replace(tzinfo=ZoneInfo(Region))
        for t in times
    ]
    dt_utc = [t.astimezone(pytz.utc) for t in dt]
    times_utc = [t.strftime("%Y-%m-%d %H:%M:%S") for t in dt_utc]
    AZ = [Location(L).solar_azimuth(t) for t in dt]

    # split day and time into separate strings for saving
    the_date = []
    the_times = []
    for t in times:
        DateAndTime = t.split(" ")
        the_date.append(DateAndTime[0])
        the_times.append(DateAndTime[1])

    UTC_date = []
    UTC_times = []
    for t in times_utc:
        DateAndTime = t.split(" ")
        UTC_date.append(DateAndTime[0])
        UTC_times.append(DateAndTime[1])

    data = {
        "Date/Local": the_date,
        "Time/Local": the_times,
        "Date/UTC": UTC_date,
        "Time/UTC": UTC_times,
        "Solar Azimuth/deg": AZ,
    }
    df = pd.DataFrame(data)

    # Save the Solar Azimuth Data
    filestr = f"{Loc}/Inputs/SolAz_LUTs/{strDate}_{Loc}.csv"
    df.to_csv(filestr, index=False)

    return 0


def lat_long_gen(nadLat, nadLong, heading, deflect, OWL_Angle, nadirHeight):
    """
    Homebrew Haversine calculation. Will come back to this.
    """
    R = 6378.1  # Radius of the Earth
    C = math.pi / 180
    brng = (heading + deflect) * C
    OWL_Angle = OWL_Angle * C
    d = nadirHeight * math.sin(OWL_Angle) / 1000  # km
    height = nadirHeight * math.cos(OWL_Angle)  # m

    lat1 = math.radians(nadLat)  # Current lat point converted to radians
    lon1 = math.radians(nadLong)  # Current long point converted to radians

    lat2 = math.asin(
        math.sin(lat1) * math.cos(d / R)
        + math.cos(lat1) * math.sin(d / R) * math.cos(brng)
    )

    lon2 = lon1 + math.atan2(
        math.sin(brng) * math.sin(d / R) * math.cos(lat1),
        math.cos(d / R) - math.sin(lat1) * math.sin(lat2),
    )

    # Convert back to degrees
    lat2 = math.degrees(lat2)
    lon2 = math.degrees(lon2)

    return [lat2, lon2, d, height]


# =================================================================================================================
"""
Heading Operations

"""


def recip_Heading(heading):
    """
    Returns the reciprocal heading
    Inputs: Heading (degrees)
    """
    if heading > 360 or heading <= 0:
        return (
            "invalid heading detected - heading defined as 0 < heading <= 360"
        )
        moan = (
            "invalid heading detected - heading defined as 0 < heading <= 360"
        )
        raise ValueError(moan)
    return heading + 180 if heading < 180 else heading - 180


def deflection_heading(heading, angle_def):
    """
    Get the heading which is offset from current heading by a set amount

    Inputs: heading (degrees), angle_def (degrees)
    Define clockwise as the positive direction
    """
    # Add offset
    new_heading = heading + angle_def

    # Bring new heading back into defined range (0 < heading <= 360)
    if new_heading > 360:
        new_heading = new_heading - 360
    elif new_heading <= 0:
        new_heading = new_heading + 360
    else:
        new_heading = new_heading
    return new_heading


# =================================================================================================================
"""
Flight Line generation for multi-angular studies
"""


def multi_ang_FLGen(
    targLat, targLong, heading, length, FOV, nadirHeight, VZA, skyGonMode
):
    """
    Generate flight line for multi-angular survey over target lat/long

    Inputs: targlat/long (decimal degrees), heading (degrees clockwise from north),
    flight line length km), FOV (degrees), nadirHeight (metres), skyGonMode (True/False)
    - allows for alterable flightline height to keep atmospheric column constant
    """

    # Get offset lat/long from the target from desired VZA
    # Define -ve as towards the left of the nadir flight line

    if VZA == 0:
        targLat = targLat
        targLong = targLong
        height = nadirHeight
        sep_left = height * tan(radians(VZA - (FOV / 2))) / 1000
        sep_right = height * tan(radians((FOV / 2) - VZA)) / 1000
        new_heading = deflection_heading(heading, 90)
        leftLat, leftLong = H.inverse_haversine(
            (targLat, targLong), sep_left, radians(np.abs(new_heading))
        )
        rightLat, rightLong = H.inverse_haversine(
            (targLat, targLong), sep_right, radians(np.abs(new_heading))
        )
    elif VZA > 0:  #########
        # Alter height and separation according to survey mode
        # Also get separation of edge of swath lines, sep_left/sep_right
        if skyGonMode is True:
            height = nadirHeight * cos(radians(VZA))  # m
            separation = nadirHeight * sin(radians(VZA)) / 1000  # km
            sep_left = height * tan(radians(VZA - (FOV / 2))) / 1000
            sep_right = height * tan(radians(VZA + (FOV / 2))) / 1000
        elif skyGonMode is False:
            height = nadirHeight
            separation = nadirHeight * tan(radians(VZA)) / 1000  # km
            sep_left = height * tan(radians(VZA - (FOV / 2))) / 1000
            sep_right = height * tan(radians(VZA + (FOV / 2))) / 1000
        else:
            moan = "WARNING: Invalid input for skyGonMode, must be either True or False"
            raise ValueError(moan)

        new_heading = deflection_heading(heading, 90)
        location = (
            targLat,
            targLong,
        )  # dump site location to another variable
        targLat, targLong = H.inverse_haversine(
            location, separation, radians(new_heading)
        )
        leftLat, leftLong = H.inverse_haversine(
            (targLat, targLong), -sep_left, radians(new_heading)
        )
        rightLat, rightLong = H.inverse_haversine(
            (targLat, targLong), -sep_right, radians(new_heading)
        )
    elif VZA < 0:
        VZA = sqrt(VZA**2)  # get absolute value
        # Alter height if sky goiniometer mode is required
        # Also get separation of edge of swath lines, sep_left/sep_right
        if skyGonMode is True:
            height = nadirHeight * cos(radians(VZA))  # m
            separation = nadirHeight * sin(radians(VZA)) / 1000  # km
            sep_left = height * tan(radians(VZA - (FOV / 2))) / 1000
            sep_right = height * tan(radians(VZA + (FOV / 2))) / 1000
        elif skyGonMode is False:
            height = nadirHeight
            separation = nadirHeight * tan(radians(VZA)) / 1000  # km
            sep_left = height * tan(radians(VZA - (FOV / 2))) / 1000
            sep_right = height * tan(radians(VZA + (FOV / 2))) / 1000
        else:
            moan = "WARNING: Invalid input for skyGonMode, must be either True or False"
            raise ValueError(moan)

        new_heading = deflection_heading(heading, -90)
        location = (targLat, targLong)
        targLat, targLong = H.inverse_haversine(
            location, separation, radians(new_heading)
        )
        leftLat, leftLong = H.inverse_haversine(
            (targLat, targLong), -sep_left, radians(new_heading)
        )
        rightLat, rightLong = H.inverse_haversine(
            (targLat, targLong), -sep_right, radians(new_heading)
        )

    # define target as line midpoint and get recip. heading.
    target = (targLat, targLong)
    target_left = (leftLat, leftLong)
    target_right = (rightLat, rightLong)
    recip = recip_Heading(heading)

    # Compute start and end lat/long and height and return as list
    startLat, startLong = H.inverse_haversine(
        target, length / 2, radians(recip)
    )
    endLat, endLong = H.inverse_haversine(
        target, length / 2, radians(heading)
    )
    startLeftLat, startLeftLong = H.inverse_haversine(
        target_left, length / 2, radians(recip)
    )
    endLeftLat, endLeftLong = H.inverse_haversine(
        target_left, length / 2, radians(heading)
    )

    startRightLat, startRightLong = H.inverse_haversine(
        target_right, length / 2, radians(recip)
    )
    endRightLat, endRightLong = H.inverse_haversine(
        target_right, length / 2, radians(heading)
    )

    return [
        startLat,
        startLong,
        startLeftLat,
        startLeftLong,
        startRightLat,
        startRightLong,
        int(height),
        endLat,
        endLong,
        endLeftLat,
        endLeftLong,
        endRightLat,
        endRightLong,
        int(height),
    ]


def searchSolAz(AOI, date, time, local_or_UTC):
    """
    Function to search for required Sol. Az. Angle from LUTs

    Inputs: AOI (e.g."Grosetto"), date ("YYYY-MM-DD"), time ("HH:MM:SS"),
    local_or_UTC (e.g. "Local" or "UTC", denotes search column used).

    Requires the folder "SolAz_LUTs" to be in the directory, updates with LUTs
    for the AOI between the desired survey period

    """
    # create search datetime object
    searchtime = pd.to_datetime(date + " " + time)

    # Create LUT file string
    file = f"{AOI}/Inputs/SolAz_LUTs/{date}_{AOI}.csv"

    # Load LUT
    data = pd.read_csv(file)

    time_column = "Time/%s" % local_or_UTC
    date_column = "Date/%s" % local_or_UTC
    data["search time"] = pd.to_datetime(
        data[date_column] + " " + data[time_column]
    )

    # Search the data frame for the nearest time and extract corresponding
    # Solar Azimuth Angle
    search_times = data["search time"].tolist()

    res = min(enumerate(search_times), key=lambda x: abs(searchtime - x[1]))
    index = res[0]
    AZ = data["Solar Azimuth/deg"].tolist()
    AZ = AZ[index]

    return int(AZ)


def ma_FLSet_Gen(
    Ident,
    targLat,
    targLong,
    heading,
    length,
    FOV,
    nadirHeight,
    VZAList,
    skyGonMode,
    degen,
    outputDir,
    date,
):
    """
    Generate Multi-Angular flight line set from list of desired View Zenith Angles.
    Outputs to both XYZ and KML
    """

    # Filter out degenerate flight lines if so desired
    if degen is True:
        VZAList = VZAList
    elif degen is False:
        VZAList = set(VZAList)

    # Initialise Lists
    FL_Name_list = []
    start_lat_list = []
    start_long_list = []
    end_lat_list = []
    end_long_list = []

    start_left_lat_list = []
    start_left_long_list = []
    start_right_lat_list = []
    start_right_long_list = []

    end_left_lat_list = []
    end_left_long_list = []
    end_right_lat_list = []
    end_right_long_list = []

    height_list = []

    start_lat_long = []
    end_lat_long = []
    FL_Name_table = []
    line_dir = []
    height_feet = []
    length_list = []
    time_list = []

    c = 1
    for VZA in VZAList:
        line_info = multi_ang_FLGen(
            targLat,
            targLong,
            heading,
            length,
            FOV,
            nadirHeight,
            VZA,
            skyGonMode,
        )
        FL_Name_list.append(f"{Ident}_{VZA}")
        start_lat_list.append(line_info[0])
        start_long_list.append(line_info[1])
        start_left_lat_list.append(line_info[2])
        start_left_long_list.append(line_info[3])
        start_right_lat_list.append(line_info[4])
        start_right_long_list.append(line_info[5])
        height_list.append(line_info[6])
        end_lat_list.append(line_info[7])
        end_long_list.append(line_info[8])
        end_left_lat_list.append(line_info[9])
        end_left_long_list.append(line_info[10])
        end_right_lat_list.append(line_info[11])
        end_right_long_list.append(line_info[12])

        start_lat_long.append(
            deg_to_degString(line_info[0], line_info[1], "dec_deg")
        )
        end_lat_long.append(
            deg_to_degString(line_info[7], line_info[8], "dec_deg")
        )
        line_dir.append(heading)
        height_feet.append(conv_metToFeet(line_info[6], "met_to_feet"))
        FL_Name_table.append("A0%s" % c)
        length_list.append(length)
        time_list.append(round(length * 1000 / 62, 1))

        c = c + 1

    # Create waypoint list
    waypoint_lat = []
    waypoint_long = []
    waypoint = []
    count = 0
    for i in range(len(start_lat_list)):
        waypoint_lat.append(start_lat_list[i])
        waypoint_lat.append(end_lat_list[i])
        waypoint_long.append(start_long_list[i])
        waypoint_long.append(end_long_list[i])
        count = count + 1
    for i in range(len(waypoint_lat)):
        waypoint.append("WAY%s" % i)

    # data1 = {"FL Name": FL_Name_list, "Start Lat/degrees": start_lat_list,
    #        "Start Long/degrees": start_long_list, "Height/m": height_list,
    #        "End Lat/degrees": end_lat_list, "End Long/degrees": end_long_list,
    #         "Height/m": height_list}

    # df1 = pd.DataFrame(data1)

    # data2 = {"FL Name": FL_Name_list, "Start Long/degrees": start_long_list,
    #         "Start Lat/degrees": start_lat_list, "Height/m": height_list,
    #         "End Long/degrees": end_long_list, "End Lat/degrees": end_lat_list,
    #         "Height/m": height_list}

    # df2 = pd.DataFrame(data2)

    data3 = {
        "Flight Line Label": FL_Name_table,
        "Line Direction [deg]": line_dir,
        "Begin WGS84 [Default]": start_lat_long,
        "End WGS84 [Default]": end_lat_long,
        "Alt MSL [ft]": height_feet,
        "Length [km]": length_list,
        "Estimated Flight Time [s]": time_list,
    }

    df3 = pd.DataFrame(data3)

    # data4 = {"Waypoint ID": waypoint, "Latitude": waypoint_lat,
    #         "Longitude": waypoint_long}

    # df4 = pd.DataFrame(data4)

    # Generate KML
    # print("Generate KML")
    geoj = create_geojson(
        FL_Name_list,
        start_long_list,
        start_lat_list,
        height_list,
        end_long_list,
        end_lat_list,
        start_left_long_list,
        start_left_lat_list,
        start_right_long_list,
        start_right_lat_list,
        end_left_long_list,
        end_left_lat_list,
        end_right_long_list,
        end_right_lat_list,
    )
    kml = create_kml(
        FL_Name_list,
        start_long_list,
        start_lat_list,
        height_list,
        end_long_list,
        end_lat_list,
        start_left_long_list,
        start_left_lat_list,
        start_right_long_list,
        start_right_lat_list,
        end_left_long_list,
        end_left_lat_list,
        end_right_long_list,
        end_right_lat_list,
    )
    # Get day of the year from date string
    doy = int(dt.datetime.strptime(date, "%Y-%m-%d").strftime("%j"))
    # generate Output file String
    degen_str = "degen" if degen else "non-degen"
    skygon_str = "SkyGon" if skyGonMode else "Standard"

    fileString = f"{doy}_{outputDir}_{Ident}-{skygon_str}_{degen_str}"
    # kml.save
    fileKML = fileString + ".kml"
    filejson = fileString + ".geojson"
    with Path(filejson).open(mode="w") as fp:
        geojson.dump(geoj, fp, sort_keys=True)

    kml.save(fileKML)
    # fileKML = fileString + "WAYPOINTS" + ".kml"
    # kml.save(fileKML)
    fileCSV = fileString + ".csv"
    df3.to_csv(fileCSV)

    return geoj


def ma_FLSet_SolTrack(
    Ident,
    targLat,
    targLong,
    length,
    nadirHeight,
    VZAList,
    skyGonMode,
    degen,
    outputDir,
    date,
    start_time,
    survey_speed,
    turn_time,
):
    """
    Generate Multi-Angular flight line set from list of desired View Zenith Angles.
    Outputs to both XYZ and KML
    Tracks the sun as the survey goes on.
    """

    # Get end time from start time and number of flight lines in a single set.
    start_time = pd.to_datetime(date + " " + start_time)
    # Strip date from the string
    # date_split = date.split("-")
    # year = int(date_split[0])
    # month = int(date_split[1])
    # day = int(date_split[2])

    # Convert to datetime object and generate the time series
    #
    FL_N = len(VZAList)
    survey_speed = (
        survey_speed * 0.514444
    )  # Convert aircraft speed from knots to m/s
    FL_t = (
        length * 1000 / survey_speed + turn_time * 60
    )  # Time to fly one FL and turn at the end
    total_time = (FL_N - 1) * FL_t

    times = [
        dt.strftime("%Y-%m-%d %H:%M:%S")
        for dt in datetime_range(
            start_time,
            start_time + timedelta(seconds=total_time),
            timedelta(seconds=FL_t),
        )
    ]

    # Initialise Lists
    FL_Name_list = []
    start_lat_list = []
    start_long_list = []
    end_lat_list = []
    end_long_list = []
    height_list = []

    c = 0
    for t in times:
        if Ident == "SP":
            FL_time = t.split(" ")
            heading = searchSolAz(outputDir, FL_time[0], FL_time[1], "UTC")

        elif Ident == "AZ":
            FL_time = t.split(" ")
            heading = searchSolAz(outputDir, FL_time[0], FL_time[1], "UTC")
            heading = deflection_heading(heading, -90)
        else:
            moan = 'WARNING: Invalid Line Ident., must be "SP" or "AZ"'
            raise ValueError(moan)

        line_info = multi_ang_FLGen(
            targLat,
            targLong,
            heading,
            length,
            nadirHeight,
            VZAList[c],
            skyGonMode,
        )
        FL_Name_list.append(f"{Ident}_{VZAList[c]}")
        start_lat_list.append(line_info[0])
        start_long_list.append(line_info[1])
        height_list.append(line_info[2])
        end_lat_list.append(line_info[3])
        end_long_list.append(line_info[4])

        c = c + 1

    # Generate KML
    kml = simplekml.Kml()
    c = 0
    for i in range(len(FL_Name_list)):
        ls = kml.newlinestring(name=FL_Name_list[c])
        ls.coords = [
            (start_long_list[c], start_lat_list[c], height_list[c]),
            (end_long_list[c], end_lat_list[c], height_list[c]),
        ]
        ls.altitudemode = simplekml.AltitudeMode.relativetoground
        ls.style.linestyle.width = 2
        ls.style.linestyle.color = simplekml.Color.red
        c = c + 1

    # Get day of the year from date string
    # doy = datetoDOY(date, leap)
    doy = int(dt.datetime.strptime(date, "%Y-%m-%d").strftime("%j"))
    # generate Output file String
    degen_str = "degen" if degen else "non-degen"
    skygon_str = "SkyGon" if skyGonMode else "Standard"

    fileString = (
        f"{doy}_{outputDir}_{Ident}-SolTrack-{skygon_str}_{degen_str}"
    )

    fileKML = fileString + ".kml"
    kml.save(fileKML)

    # TO DO TOMORROW: Save as KML, auto-detect start time from csv maybe??
    # do we want 2 degree variation between VZA and its nadir???

    return total_time


# def nearest(ts):
#    s = sorted(ts_list)
#    i = bisect_left(s, ts)
#    return min(s[max(0, i-1): i+2], key=lambda t: abs(ts - t))
def read_flight_line(inputfile, targLong, targLat, date):
    # Read in Flight line data form
    #    inputfile = "%s/Inputs/%s_%s_FL-input.csv" % (AOI, date, AOI)
    FL = (
        pd.read_csv(inputfile)
        if isinstance(inputfile, (str, Path))
        else inputfile
    )
    # Parse data frame according to identifier
    LSTM = FL[FL["Ident"] == "LSTM"]
    SP = FL[FL["Ident"] == "SP"]
    AZ = FL[FL["Ident"] == "AZ"]

    # Get heading according to identifier
    LSTM_Head = 13

    SP_Time = SP["Time/UTC"].dropna().to_numpy()

    SP_Head = get_solar_azimuth(SP_Time, targLong, targLat, date)[0]

    AZ_Time = AZ["Time/UTC"].dropna().to_numpy()
    AZ_Head = get_solar_azimuth(AZ_Time, targLong, targLat, date)[0]
    AZ_Head = deflection_heading(AZ_Head, -90)

    return LSTM, SP, AZ, LSTM_Head, SP_Head, AZ_Head


def get_solar_azimuth(time, targLong, targLat, date):
    times = [
        dt.datetime.strptime(f"{date} {timex}", "%Y-%m-%d %H:%M")
        for timex in time
    ]
    saa = [
        calculate_solar_position(targLong, targLat, timex)[1]
        for timex in times
    ]

    return [int(x) for x in saa]


def do_LSTM_flight_lines(
    inputfile,
    AOI,
    date,
    targLat,
    targLong,
    length,
    nadirHeight,
    skyGonMode,
    degen,
    FOV,
):
    LSTM, SP, AZ, LSTM_Head, SP_Head, AZ_Head = read_flight_line(
        inputfile, targLong, targLat, date
    )

    # LSTM Flight Lines
    VZAList = LSTM["VZA"].to_numpy()

    geojson_LSTM = ma_FLSet_Gen(
        "LSTM",
        targLat,
        targLong,
        LSTM_Head,
        length,
        FOV,
        nadirHeight,
        VZAList,
        skyGonMode,
        degen,
        AOI,
        date,
    )

    # SP Flight Lines
    VZAList = SP["VZA"].to_numpy()
    geojson_SP = ma_FLSet_Gen(
        "SP",
        targLat,
        targLong,
        SP_Head,
        length,
        FOV,
        nadirHeight,
        VZAList,
        skyGonMode,
        degen,
        AOI,
        date,
    )

    # AZ Flight Lines
    VZAList = AZ["VZA"].to_numpy()
    geojson_AZ = ma_FLSet_Gen(
        "AZ",
        targLat,
        targLong,
        AZ_Head,
        length,
        FOV,
        nadirHeight,
        VZAList,
        skyGonMode,
        degen,
        AOI,
        date,
    )

    return geojson_LSTM, geojson_SP, geojson_AZ


if __name__ == "__main__":
    # AOI = "Grosseto"
    # date = "2022-05-18"
    # leap = "no"
    # targLat = 42.7635
    # targLong = 11.1124
    # length = 14.52
    # nadirHeight = 1000
    # skyGonMode = False
    # degen = True

    FOV = 24.2
    geojson_LSTM, geojson_SP, geojson_AZ = do_LSTM_flight_lines(
        "/tmp/Multi-Ang_FLGen/Grosseto/Inputs/2022-05-18_Grosseto_FL-input.csv",
        "Grosseto",
        date,
        targLat,
        targLong,
        length,
        nadirHeight,
        skyGonMode,
        degen,
        FOV,
    )
