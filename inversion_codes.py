from __future__ import annotations

import corner
import emcee
import matplotlib.pyplot as plt
import numpy as np
import prosail
import scipy.stats as ss

# | label: fenix-simulation
# | fig-cap: "(Top) SZA: 24.2 (Bottom) SZA: 45 deg. The different lines show values of LAI (0.5 to 5) and to different VZA angles (0 and 15 deg)"
# | fig-align: center
# | code-fold: true
# | warning: false


def gaussian(x, x0, s=4):
    y = (
        1.0
        / np.sqrt(2.0 * np.pi * s**2)
        * np.exp(-((x - x0) ** 2) / (2.0 * s**2))
    )
    y[y <= 0.005] = 0.0
    y = y / sum(y)
    return y


wv = np.arange(400, 2501)

spectral_responses = np.array(
    [
        np.fromiter((gaussian(wv, lc, 5)), float)
        for lc in np.linspace(380, 970, 148)
    ]
)


def fenix_sims(
    sza: float,
    vza: float,
    raa: float,
    prosail_params: tuple,
    simu_bands: np.ndarray,
):
    (
        n,
        cab,
        car,
        cbrown,
        cw,
        cm,
        lai,
        lidfa,
        hspot,
        lidfb,
        rsoil,
        psoil,
    ) = prosail_params
    if lidfb is not None:
        simu_refl = prosail.run_prosail(
            n,
            cab,
            car,
            cbrown,
            cw,
            cm,
            lai,
            lidfa,
            hspot,
            sza,
            vza,
            raa,
            typelidf=1,
            lidfb=lidfb,
            rsoil=rsoil,
            psoil=psoil,
        )
    else:
        # Campbell LAD
        simu_refl = prosail.run_prosail(
            n,
            cab,
            car,
            cbrown,
            cw,
            cm,
            lai,
            lidfa,
            hspot,
            sza,
            vza,
            raa,
            typelidf=2,
            lidfb=None,
            rsoil=rsoil,
            psoil=psoil,
        )
    simu_bands = simu_refl[None, :] * spectral_responses
    simu_bands = simu_bands.sum(axis=1)
    return simu_bands


# Skip first few bands
spectral_responses = spectral_responses[5:, :]
wvc = np.array([wv[s.argmax()] for s in spectral_responses])

n = 1.0
cab = 60.0
car = 10.0
cbrown = 0.05
cw = 0.0013
cm = 0.0001
lai = 4.0
lidfa = -0.35
lidfb = -0.15
hspot = 0.008
tts = 24.2
tto = 0.0
psi = 0.0
rsoil = 0.1
psoil = 0.0

fig, axs = plt.subplots(
    nrows=2, ncols=1, sharex=True, sharey=True, figsize=(6, 6)
)
for _i, tto in enumerate([0, 15]):
    for j, tts in enumerate([24.2, 45]):
        for lai in np.linspace(0.5, 5, 5):
            prosail_params = (
                n,
                cab,
                car,
                cbrown,
                cw,
                cm,
                lai,
                lidfa,
                hspot,
                lidfb,
                rsoil,
                psoil,
            )
            simu_bands = fenix_sims(
                tts, tto, psi, prosail_params, spectral_responses
            )
            axs[j].plot(wvc, simu_bands, "-")
        axs[j].set_title(f"SZA: {tts:3.1f}")

vza = 15.0
sza = 24.2
raa = 0.0


prosail_params = (
    2.0,
    40.0,
    10.0,
    0.35,
    0.001,
    0.01,
    4,
    45,
    0.05,
    None,
    0.15,
    0.0,
)
true_fenix_data = fenix_sims(tts, tto, psi, prosail_params, spectral_responses)
noisy_fenix_data = (
    true_fenix_data
    + np.random.randn(true_fenix_data.shape[0]) * true_fenix_data * 0.02
)
plt.figure()
plt.plot(wvc, true_fenix_data)
plt.plot(wvc, noisy_fenix_data)


vza = 15.0
sza = 24.2
raa = 0.0


def trunc_norm(mu, sigma):
    return ss.truncnorm(loc=mu, scale=sigma, a=-2.5, b=2.5)


log_prior = [
    trunc_norm(40, 30),
    trunc_norm(0.5, 0.5),
    trunc_norm(0.005, 0.0015),
    trunc_norm(2, 3),
    trunc_norm(45, 30),
    trunc_norm(0.1, 0.05),
]


def logpdf(
    x: tuple,
    sza: float,
    vza: float,
    raa: float,
    log_prior: list,
    rho_obs: np.ndarray,
    spectral_responses: np.ndarray,
) -> float:
    (
        cab,
        cbrown,
        cm,
        lai,
        lidfa,
        rsoil,
    ) = x
    if np.any(x <= 0):
        return -np.inf
    if (cab < 0.46) or (cab > 0.99):
        return -np.inf
    if (cbrown < 0) or (cbrown > 1):
        return -np.inf
    if (cm < 0.037) or (cm > 0.94):
        return -np.inf
    if (lai < 0.05) or (lai > 1.0):
        return -np.inf
    if (lidfa < 0.1) or (lidfa > 0.99):
        return -np.inf
    cab = -100 * np.log(cab)
    cm = (-1.0 / 100) * np.log(cm)
    lai = -2 * np.log(lai)
    lidfa = 90 * lidfa
    car = cbrown * 0.25
    hspot = 0.05
    psoil = 0.0
    n = 2.0
    lidfb = None
    xx = np.array([cab, cbrown, cm, lai, lidfa, rsoil])
    log_priors = [lp.logpdf(xx[i]) for i, lp in enumerate(log_prior)]
    if np.isnan(log_priors).any():
        return -np.inf
    if np.any(xx <= 0):
        return -np.inf
    prosail_params = (
        n,
        cab,
        car,
        cbrown,
        cw,
        cm,
        lai,
        lidfa,
        hspot,
        lidfb,
        rsoil,
        psoil,
    )

    # In this case, we use the same values for the non-inverted parameters
    # that were used for the creation of the data

    rho_pred = fenix_sims(sza, vza, raa, prosail_params, simu_bands)
    # Residual predicted - measrued
    xx = rho_pred - rho_obs
    # Figure out noise.
    sigma_obs = rho_obs * 0.05
    # Return the log-likelihood
    log_likelihood = -(0.5 * xx**2 / sigma_obs**2).sum()
    if not np.isfinite(log_likelihood):
        return -np.inf
    return log_likelihood + np.sum(log_priors)


# Test logposterior
# initial_value = [40, 0.01, 0.005, 4, 45.0, 0.1]

# print(
#     logpdf(
#         initial_value, vza, sza, raa, log_prior, true_fenix_data, simu_bands
#     )
# )


def logposterior(x):
    return logpdf(
        x,
        vza,
        sza,
        raa,
        log_prior,
        noisy_fenix_data,
        spectral_responses[::11, :],
    )


# Set up an initial value for the metropolisH chain. Far away from the truth
initial_value = [
    np.exp(-20 / 100.0),
    0.5,
    np.exp(-100 * 0.005),
    np.exp(-4 / 2.0),
    45.0 / 90.0,
    0.1,
]
# Select iteration
# If this takes too long, reduce this number, but I don't think you want to
# go below 500 or so
n_iterations = 100
ndim, nwalkers = 6, 100
p0 = np.random.randn(nwalkers, ndim) * 0.05 + np.array(initial_value)[None, :]
p0[:, 1] = np.abs(p0[:, 1])

sampler = emcee.EnsembleSampler(nwalkers, ndim, logposterior)
sampler.run_mcmc(p0, n_iterations)

vals = sampler.get_chain(flat=True)
# Convert posterior sample units back to normal values
output = vals * 1.0
output[:, 0] = -100 * np.log(vals[:, 0])
output[:, 2] = -(1.0 / 100) * np.log(vals[:, 2])
output[:, 3] = -2 * np.log(vals[:, 3])
output[:, 4] = 90 * vals[:, 4]


fig = corner.corner(
    output,
    labels=[
        r"$C_{ab}$",
        r"$C_{brown}$",
        r"$C_{dm}$",
        r"$LAI$",
        r"$ALA$",
        r"$\rho_{soil}$",
    ],
    truths=[40, 0.35, 0.001, 4, 45.0, 0.1],
)
